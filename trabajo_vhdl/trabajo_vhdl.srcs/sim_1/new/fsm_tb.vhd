----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 29.12.2020 17:40:49
-- Design Name: 
-- Module Name: fsm_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity fsm_tb is
end fsm_tb;

architecture Behavioral of fsm_tb is
    component fsm is
        port(
            clk:    in std_logic; --se�al de reloj
            --se�ales de productos:
            prod:   in std_logic_vector(3 downto 0);
            --sensor de producto recogido:
            sens:   in std_logic;
            --reset del sistema lleva a estado S0
            reset:  in std_logic;
            --cuenta contador:
            cont:   in std_logic_vector(7 downto 0);
            LED:    out std_logic_vector(3 downto 0);
            --codigo para 7 segmentos (misma longitud que contador)
            code:   out std_logic_vector(7 downto 0);
            --reset para el contador (LOGICA NEGADA)
            c_reset:out std_logic
        );
    end component;
    --se�ales:
    --inputs:
    signal clk:     std_logic:='0';
    signal prod:    std_logic_vector(3 downto 0);
    signal sens:    std_logic:='0';
    signal reset:   std_logic:='0';
    signal cont:    std_logic_vector(7 downto 0);
    --outputs:
    signal LED:     std_logic_vector(3 downto 0);
    signal code:    std_logic_vector(7 downto 0);
    signal c_reset: std_logic;
--definimos constante de periodo:
constant clk_period: time:= 1sec/1000; --1ms
  
begin
--instanciamos componente:
    uut: fsm
        port map(
        clk=>      clk,
        prod=>     prod,   
        sens=>     sens,   
        reset=>    reset,   
        cont=>     cont,
        LED=>      LED,
        code=>     code,
        c_reset=>  c_reset
        );
--se�al de reloj:
    clk <=not(clk) after clk_period/2;
--se�al reset:
    reset <='1' after 20.5*clk_period, '0' after 21*clk_period;
--cuenta:
    cont <= (others=>'0'),"00001010" after 7.5*clk_period,"00111100" after 8.5*clk_period,"01100100" after 9.5*clk_period,"10111110" after 25.5*clk_period;
--sensor:
 sens <='1'after 14.5*clk_period,'0'after 15.5*clk_period;
--productos:
    process
        begin
        prod <= (others=>'0');
        wait for 1.5*clk_period;
        prod <= "0001";
        wait for clk_period;
        prod <= "0010";
        wait for clk_period;
        prod <= "0100";
        wait for clk_period;
        prod <= "1000";
        wait for clk_period;
        prod <= "1000";
        wait for clk_period;
    end process;
end Behavioral;
