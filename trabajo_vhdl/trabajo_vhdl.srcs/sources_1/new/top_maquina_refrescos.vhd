----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 29.12.2020 20:32:16
-- Design Name: 
-- Module Name: top_maquina_refrescos - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity top_maquina_refrescos is
--DE MOMENTO EN PRUEBAS HASTA METER CONTROLADOR DEL 7 SEGMENTOS
    port (
        clk:    in std_logic;
        prod:   in std_logic_vector(3 downto 0);
        coins:  in std_logic_vector(3 downto 0);
        sens:   in std_logic;
        reset:  in std_logic;
        --estas salidas no son las definitivas son solo para los test hasta ocntrol de 7 seg.
        LEDs:   out std_logic_vector(3 downto 0);
        code:   out std_logic_vector(7 downto 0)
    );
end top_maquina_refrescos;

architecture Behavioral of top_maquina_refrescos is
--array de sincronizadores:
    component sinc_array
        port(
            clk:            in std_logic;
            asinc_array:    in std_logic_vector(9 downto 0);
            sinc_array:     out std_logic_vector(9 downto 0)
        );
    end component;
--array de edge detectors:
    component edge_array
        port(
            clk:        in std_logic;
            edge_in:    in std_logic_vector(9 downto 0);
            edge_out:   out std_logic_vector(9 downto 0)
        );
    end component;
--contador:
    component Contador
        port(
            n_reset:in std_logic; 
            clk:    in std_logic; 
            m_10:   in std_logic;
            m_20:   in std_logic;
            m_50:   in std_logic;
            m_100:  in std_logic;
            cout:   out std_logic_vector(7 downto 0)     
        );
    end component;
--m�quina de estados:
    component fsm
        port(
            clk:    in std_logic; 
            prod:   in std_logic_vector(3 downto 0);
            sens:   in std_logic;
            reset:  in std_logic;
            cont:   in std_logic_vector(7 downto 0);
            LED:    out std_logic_vector(3 downto 0);
            code:   out std_logic_vector(7 downto 0);
            c_reset:out std_logic
        );
    end component;
--se�ales de conexion:
    signal sinc_to_edge:    std_logic_vector(9 downto 0);
    
    signal prod_s:          std_logic_vector(3 downto 0); --(3 2 1 0)->(P4 P3 P2 P1)
    signal coins_s:         std_logic_vector(3 downto 0); --(3 2 1 0)->(M100 M50 M20 M10)
    signal sens_s:          std_logic;
    signal reset_s:         std_logic;  
    
    signal count_reset:     std_logic;
    signal cuenta_s:        std_logic_vector(7 downto 0);
begin
--inicializamos los componentes:
    sincronizador: sinc_array
    port map(
        clk => clk,
        asinc_array(9 downto 6)=>prod,
        asinc_array(5 downto 2)=>coins,
        asinc_array(1)=>sens,
        asinc_array(0)=>reset,
        sinc_array=>sinc_to_edge
    );
    edge_detector: edge_array
    port map(
        clk=>clk,
        edge_in=>sinc_to_edge, 
        edge_out(9 downto 6)=>prod_s,
        edge_out(5 downto 2)=>coins_s,
        edge_out(1)=>sens_s,
        edge_out(0)=>reset_s
    );
    contador_unit: Contador
    port map(
        n_reset=>count_reset, 
        clk=>clk,  
        m_10=>coins_s(0),
        m_20=>coins_s(1),
        m_50=>coins_s(2),
        m_100=>coins_s(3),
        cout => cuenta_s
    );
    fsm_unit: fsm
    port map(
        clk=>clk, 
        prod=>prod_s,
        sens=>sens_s,
        reset=>reset_s,
        cont=>cuenta_s,
        LED=>LEDs,
        code=>code,
        c_reset=>count_reset 
    );

end Behavioral;
