----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 29.12.2020 11:14:57
-- Design Name: 
-- Module Name: contador_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity contador_tb is
end contador_tb;

architecture Behavioral of contador_tb is
    --declaramos el componente contador:
    component Contador is
        port(
            n_reset:in std_logic; --reset LOGICA NEGADA (cuando 0 se resetea)
            clk:    in std_logic; --se�al de reloj
            --se�ales de monedas (las pongo por separado porque se entiende mejor pero podr�an ser un vector)
            m_10:   in std_logic;
            m_20:   in std_logic;
            m_50:   in std_logic;
            m_100:  in std_logic;
            cout:   out std_logic_vector(7 downto 0) --salida del contador (9 bits m�xima cuenta 255)
        );
    end component; 
    --se�ales (las mismas):
    --inputs:
    signal n_reset: std_logic:= '0'; --iniciamos a 0
    signal clk:     std_logic:= '0'; --iniciamos a 0
    signal m_10:    std_logic:= '0';
    signal m_20:    std_logic:= '0';
    signal m_50:    std_logic:= '0';
    signal m_100:   std_logic:= '0';  
    --outputs:
    signal cout:    std_logic_vector(7 downto 0);
    --definimos periodo de reloj:
    constant clk_period: time:= 1sec/1000; --1ms
begin
    --instanciamos componente:
    uut: Contador
        port map(
            n_reset => n_reset,
            clk => clk,
            m_10 => m_10,
            m_20 => m_20,
            m_50 => m_50,
            m_100 => m_100,
            cout => cout
        );
    clk <= not(clk) after clk_period/2; --se�al de reloj
    n_reset <= '1' after 1.5*clk_period,'0' after 6.5*clk_period,'1' after 7.5*clk_period; 
    
    --se�ales de contador:
    m_10 <= '1' after 2.5*clk_period, '0' after 3*clk_period;
    m_20 <= '1' after 4.5*clk_period, '0' after 5*clk_period;
    m_50 <= '1' after 5.5*clk_period, '0' after 6*clk_period;
    m_100 <= '1' after 7.5*clk_period, '0' after 8*clk_period;
    process
    begin
        wait for 25*clk_period;
        assert false
            report "[SUCCESS]: Simulation finished."
            severity failure;
    end process;
end Behavioral;
