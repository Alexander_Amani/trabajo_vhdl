----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 29.12.2020 21:29:32
-- Design Name: 
-- Module Name: top_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity top_tb is
--  Port ( );
end top_tb;

architecture Behavioral of top_tb is
    component top_maquina_refrescos
        port(
            clk:    in std_logic;
            prod:   in std_logic_vector(3 downto 0);
            coins:  in std_logic_vector(3 downto 0);
            sens:   in std_logic;
            reset:  in std_logic;
            --estas salidas no son las definitivas son solo para los test hasta ocntrol de 7 seg.
            LEDs:   out std_logic_vector(3 downto 0);
            code:   out std_logic_vector(7 downto 0)
        );
    end component;
--definimos el periodo:
    constant clk_period: time:= 1sec/1000; --1ms
--definimos las se�ales:
   signal clk:  std_logic:='0';
   signal prod: std_logic_vector(3 downto 0);
   signal coins:std_logic_vector(3 downto 0);
   signal sens: std_logic:='0';
   signal reset:std_logic:='0';
    --estas salidas no son las definitivas son solo para los test hasta ocntrol de 7 seg.
   signal LEDs: std_logic_vector(3 downto 0);
   signal code: std_logic_vector(7 downto 0);
begin
--instanciamos componente:
    uut: top_maquina_refrescos
    port map(
        clk=>clk,
        prod=>prod,
        coins=>coins,
        sens=>sens,
        reset=>reset,
        LEDs=>LEDs,
        code=>code
    );
--se�al de reloj:
    clk <= not(clk) after 0.5*clk_period;
--se�al reset:
--reset <='1' after 24.5*clk_period, '0' after 27.5*clk_period;
--se�al sensor:
    sens <='1' after 44.5*clk_period, '0' after 45.5*clk_period;
--se�al monedas:
    coins <="0000","0100" after 25.5*clk_period, "0000" after 29*clk_period, "1000" after 32*clk_period,"0000" after 34*clk_period;
--productos:
    process
        begin
        prod <= (others=>'0');
        wait for 2*clk_period;
        prod <= "0001";
        wait for 3*clk_period;
        prod <= "0000";
        wait for 2*clk_period;
        prod <= "0010";
        wait for 3*clk_period;
        prod <= "0000";
        wait for 2*clk_period;
        prod <= "0100";
        wait for 3*clk_period;
        prod <= "0000";
        wait for 2*clk_period;
        prod <= "1000";
        wait for 3*clk_period;
        prod <= "0000";
        wait for 2*clk_period;
        prod <= "1000";
        wait for 3*clk_period;
        prod <= "0000";
        wait for 2*clk_period;
    end process;
--final de simulaci�n:
    process
        begin
        --simulamos 100 ciclos de reloj:
            wait for 100*clk_period;
            assert false
                report "[SUCCESS]: Simulation finished."
                severity failure;
    end process;

end Behavioral;
