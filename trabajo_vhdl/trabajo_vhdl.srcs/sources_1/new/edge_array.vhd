----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 29.12.2020 20:17:19
-- Design Name: 
-- Module Name: edge_array - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity edge_array is
    port(
        clk:        in std_logic;
        edge_in:    in std_logic_vector(9 downto 0);
        edge_out:   out std_logic_vector(9 downto 0)
    );
end edge_array;

architecture Behavioral of edge_array is
    component edge
        port(
            CLK : in std_logic;
            SYNC_IN : in std_logic;
            EDGE : out std_logic
        );
    end component;
begin
    GEN_EDGE:
    for i in 9 downto 0 generate
        uut: edge
        port map(
           CLK => clk,
           SYNC_IN => edge_in(i),
           EDGE => edge_out(i)
        );
    end generate GEN_EDGE;

end Behavioral;
