----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 16.11.2020 09:58:27
-- Design Name: 
-- Module Name: SYNCHRNZR - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity sinc is
     port (
         clk : in std_logic;
         ASYNC_IN : in std_logic;
         SYNC_OUT : out std_logic
     );
end sinc;

architecture Behavioral of sinc is
    signal sreg : std_logic_vector(1 downto 0);
begin
    process (clk)
        begin
        if rising_edge(clk) then
            sync_out <= sreg(1);
            sreg <= sreg(0) & async_in;
        end if;
        end process;
end Behavioral;