----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 29.12.2020 19:48:59
-- Design Name: 
-- Module Name: sinc_array - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity sinc_array is
    port(
        clk:            in std_logic;
        asinc_array:    in std_logic_vector(9 downto 0);
        sinc_array:     out std_logic_vector(9 downto 0)
    );
end sinc_array;

architecture Behavioral of sinc_array is
    component sinc
        port(
            clk : in std_logic;
            ASYNC_IN : in std_logic;
            SYNC_OUT : out std_logic
        );
    end component;
begin
    GEN_SINC:
    for i in 9 downto 0 generate
        uut: sinc
        port map(
            clk => clk,
            ASYNC_IN => asinc_array(i),
            SYNC_OUT => sinc_array(i)
        );
    end generate GEN_SINC;

end Behavioral;
