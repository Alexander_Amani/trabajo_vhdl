----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 29.12.2020 10:20:03
-- Design Name: 
-- Module Name: Contador - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Contador is
--  Port ( );
    port(
        n_reset:in std_logic; --reset LOGICA NEGADA (cuando 0 se resetea)
        clk:    in std_logic; --se�al de reloj
        --se�ales de monedas (las pongo por separado porque se entiende mejor pero podr�an ser un vector)
        m_10:   in std_logic;
        m_20:   in std_logic;
        m_50:   in std_logic;
        m_100:  in std_logic;
        cout:   out std_logic_vector(7 downto 0) --salida del contador (9 bits m�xima cuenta 255)    
    );
end Contador;

architecture Behavioral of Contador is
    signal cout_s: integer range 0 to 255:=0;
begin
    process(n_reset,clk) --el reset al ser salida de la m�quina de estados igual no es necesario ponerla como se�al asincrona
    begin
        if n_reset='0' then --LOGICA NEGADA
            cout_s<=0;
        elsif rising_edge(clk) then
            if m_10='1' then
                cout_s<=cout_s+10;
            elsif m_20='1' then
                cout_s<=cout_s+20;
            elsif m_50='1' then
                cout_s<=cout_s+50;
            elsif m_100='1' then
                cout_s<=cout_s+100;
            end if;    
        end if;
    end process;
    --convertimos la se�al a salida de std_logic_vector:
    cout <= std_logic_vector(to_unsigned(cout_s,cout'length));
end Behavioral;
