----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 29.12.2020 13:27:10
-- Design Name: 
-- Module Name: fsm - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity fsm is
    port(
        clk:    in std_logic; --se�al de reloj
        --se�ales de productos:
        prod:   in std_logic_vector(3 downto 0);
        --sensor de producto recogido:
        sens:   in std_logic;
        --reset del sistema lleva a estado S0
        reset:  in std_logic;
        --cuenta contador:
        cont:   in std_logic_vector(7 downto 0);
        LED:    out std_logic_vector(3 downto 0);
        --codigo para 7 segmentos (misma longitud que contador)
        code:   out std_logic_vector(7 downto 0);
        --reset para el contador (LOGICA NEGADA)
        c_reset:out std_logic
    );
end fsm;
    
architecture Behavioral of fsm is
    type STATES is (S0, S1, S2, S3, S4, S5, S6, S7, 
                    S8, S9, S10, S11, S12, S13);
    signal current_state: STATES;
	signal next_state: STATES;
begin
    state_register: process (reset, clk)
        begin
            if reset = '1' then
                current_state <= S0;
            elsif CLK'event and CLK = '1' then
                current_state <= next_state;
            end if;
        end process;
    nextstate_decod: process (cont, sens, prod, current_state)
        begin
            next_state <= current_state;
            case current_state is
                when S0 =>
                --del estado de reposo pasa a mostrar el producto seleccionado pulsado el 
                --boton que le corresponde:
                    if prod(0) = '1' then
                        next_state <= S1;
                    elsif prod(1) = '1' then
                        next_state <= S2;
                    elsif prod(2) = '1' then
                        next_state <= S3;
                    elsif prod(2) = '1' then
                        next_state <= S4;                    
                    end if;
                    
                -- S1, S2, S3, S4 muestran al cliente los dintitos productos, para confirmar volver a pulsar
                when S1 =>
                --Cuando estoy mostrando al cliente el producto 1:
                --puedo pasar a mostrar otro de los productos o confirmar que quiero ese y pasar al estado de "esperando dinero"
                    if prod(0) = '1' then --si vuelvo a pulsar por el producto 1
                        next_state <= S5; --esperando dinero
                    elsif prod(1) = '1' then --si pulso para ver el producto 2
                        next_state <= S2;
                    elsif prod(2) = '1' then --si pulso para ver el producto 3
                        next_state <= S3;
                    elsif prod(3) = '1' then --si pulso para ver el producto 4
                        next_state <= S4;
                    end if;
                when S2 =>
                --Cuando estoy mostrando al cliente el producto 2:
                --puedo pasar a mostrar otro de los productos o confirmar que quiero ese y pasar al estado de "esperando dinero"
                    if prod(1) = '1' then --si vuelvo a pulsar por el producto 2
                        next_state <= S6; --esperando dinero
                    elsif prod(0) = '1' then --si pulso para ver el producto 1
                        next_state <= S1;
                    elsif prod(2) = '1' then --si pulso para ver el producto 3
                        next_state <= S3;
                    elsif prod(3) = '1' then --si pulso para ver el producto 4
                        next_state <= S4;
                    end if;
                when S3 =>
                --Cuando estoy mostrando al cliente el producto 3:
                --puedo pasar a mostrar otro de los productos o confirmar que quiero ese y pasar al estado de "esperando dinero"
                    if prod(2) = '1' then --si vuelvo a pulsar por el producto 3
                        next_state <= S7; --esperando dinero
                    elsif prod(0) = '1' then --si pulso para ver el producto 1
                        next_state <= S1;
                    elsif prod(1) = '1' then --si pulso para ver el producto 2
                        next_state <= S2;
                    elsif prod(3) = '1' then --si pulso para ver el producto 4
                        next_state <= S4;
                    end if;
                 when S4 =>
                --Cuando estoy mostrando al cliente el producto 4:
                --puedo pasar a mostrar otro de los productos o confirmar que quiero ese y pasar al estado de "esperando dinero"
                    if prod(3) = '1' then --si vuelvo a pulsar por el producto 4
                        next_state <= S8; --esperando dinero
                    elsif prod(0) = '1' then --si pulso para ver el producto 1
                        next_state <= S1;
                    elsif prod(1) = '1' then --si pulso para ver el producto 2
                        next_state <= S2;
                    elsif prod(2) = '1' then --si pulso para ver el producto 3
                        next_state <= S3;
                    end if;
                    
                -- S5, S6, S7, S8 son estados de espera del contador, si supera 100 va a S13 error, si no entrega producto
                when S5 =>
                    if to_integer(unsigned(cont))>100 then
                        next_state <= S13;
                    elsif to_integer(unsigned(cont))=100 then
                        next_state <= S9;
                    end if;
                when S6 =>
                    if to_integer(unsigned(cont))>100 then
                        next_state <= S13;
                    elsif to_integer(unsigned(cont))=100 then
                        next_state <= S10;
                    end if;
                when S7 =>
                    if to_integer(unsigned(cont))>100 then
                        next_state <= S13;
                    elsif to_integer(unsigned(cont))=100 then
                        next_state <= S11;
                    end if; 
                when S8 =>
                    if to_integer(unsigned(cont))>100 then
                        next_state <= S13;
                    elsif to_integer(unsigned(cont))=100 then
                        next_state <= S12;
                    end if;
                
                -- S9, S10, S11, S12 son los estados en los que se entrega el producto al cliente y se vuelve al reposo:
                when S9|S10|S11|S12 => --con cualquiera de esos estados (si no funciona se pone por separado)
                    if sens = '1' then
                        next_state <= S0; 
                    end if;
                --S13 estado de error: pulsar cualquier boton producto o reset para volver a S0 (SE PUEDE CAMBIAR)
                when S13 =>
                    if prod(0)='1' or prod(1)='1' or prod(2)='1' or prod(3)='1' or reset='1' then
                        next_state <= S0;
                    end if;                                                              
                when others => --si cualquier otra cosa volvemos a reposo
                    next_state <= S0;
            end case;
        end process;
    
    --nuestras salidas son:
        -- 4LEDs que representan los 4 productos
        --el codigo que va al controlador del 7 segmentos. de 0 a 100 es la cuenta y de 101 a 114 los correspondientes estados
        --el reset CON LOGICA NEGADA del contador.    
    output_decod: process (current_state, cont) --si cambia la cuenta tenemos que actualizar la salida en S5,S6,S7,S8
        begin
            LED <= (OTHERS => '0');
            code <= std_logic_vector(TO_UNSIGNED(101,code'length));
            c_reset <= '0';
            --creo que como hemos puesto lo de arriba no hace falta que ponga en cada when si las salidas son iguales a lo de arriba
            case current_state is
                when S0 => --en reposo leds 0, contador 0 y codigo 101
                    LED <= (OTHERS => '0');
                    code <= std_logic_vector(TO_UNSIGNED(101,code'length));
                    c_reset <= '0';
                -- S1, S2, S3, S4 leds 0, contador 0 y codigos 102, 103, 104, 105 respect.
                when S1 =>
                    LED <= (OTHERS => '0');
                    code <= std_logic_vector(TO_UNSIGNED(102,code'length));
                    c_reset <= '0';
                when S2 =>
                    LED <= (OTHERS => '0');
                    code <= std_logic_vector(TO_UNSIGNED(103,code'length));
                    c_reset <= '0';
                when S3 =>
                    LED <= (OTHERS => '0');
                    code <= std_logic_vector(TO_UNSIGNED(104,code'length));
                    c_reset <= '0';
                when S4 =>
                    LED <= (OTHERS => '0');
                    code <= std_logic_vector(TO_UNSIGNED(105,code'length));
                    c_reset <= '0';
                -- S5, S6, S7, S8 el codigo es igual a la cuenta. LEDs 0 y c_reset 1 (SE ACTIVA)
                when S5|S6|S7|S8 =>
                    LED <= (OTHERS => '0');
                    code <= cont; --EN ESTA LINEA EST� LA RAZON DE QUE NO ES M�QUINA DE MOORE
                    c_reset <= '1';
                -- S9, S10, S11, S12 code 110,111,112,113. uno de los LEDs encendidos. c_reset 0
                when S9 => --producto 1 luego LED(0) 1
                    LED <= "0001";
                    c_reset <= '0';
                    code <= std_logic_vector(TO_UNSIGNED(110,code'length));
                when S10 => --producto 2 luego LED(1) 1
                    LED <= "0010";
                    c_reset <= '0';
                    code <= std_logic_vector(TO_UNSIGNED(111,code'length));
                when S11 => --producto 3 luego LED(2) 1
                    LED <= "0100";
                    c_reset <= '0';
                    code <= std_logic_vector(TO_UNSIGNED(112,code'length));
                when S12 => --producto 4 luego LED(3) 1
                    LED <= "1000";
                    c_reset <= '0';
                    code <= std_logic_vector(TO_UNSIGNED(113,code'length));
                --S13 estado de error:
                when S13 => --producto 4 luego LED(3) 1
                    LED <= (OTHERS => '0');
                    c_reset <= '0';
                    code <= std_logic_vector(TO_UNSIGNED(114,code'length));
                when others =>
                    LED <= (OTHERS => '0');
                    code <= std_logic_vector(TO_UNSIGNED(101,code'length));
                    c_reset <= '0';
            end case;
    end process;
end Behavioral;
